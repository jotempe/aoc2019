class processor:
    def __init__(self, prog):
        self.prg = prog.copy()  # working memory, initialised to a copy of the program
        self.ptr = 0            # current instruction pointer
        self.halted = False     # true if the processor encountered halt instruction

    def run(self,inp):
        # Run until either a halt or an "unsatisfied" input instruction is encountered 
    
        def getarg(argno):
            # Get value of the argument given as offset to current ptr.
            if argno == 1 :
                amode = adrmode % 10
            elif argno == 2:
                amode = (adrmode // 10) % 10
            else:
                print("Argument number incorrect")
            if amode == 0:
                return p[p[self.ptr+argno]]
            elif amode == 1:
                return p[self.ptr+argno]
            else:
                print("Address mode error")
                
        inpused = False         # Flag will be set by first intput instruction
        outp = None             # will hold the last output
        p = self.prg
        if self.halted:
            print("Error, processor already halted")
            return
            
        while p[self.ptr] % 100 != 99:
            opcode = p[self.ptr]%100
            adrmode = p[self.ptr] // 100
            if opcode == 1:
                p[p[self.ptr+3]] = getarg(1) + getarg(2)
                self.ptr += 4
            elif opcode == 2:
                p[p[self.ptr+3]] = getarg(1) * getarg(2)
                self.ptr += 4
            elif opcode == 3:
                if inpused:
                    break
                else:
                    p[p[self.ptr+1]] = inp
                    inpused = True
                self.ptr += 2
            elif opcode == 4:
                outp=(getarg(1))
                self.ptr += 2
            elif opcode == 5:
                if getarg(1) != 0:
                    self.ptr = getarg(2)
                else:
                    self.ptr += 3
            elif opcode == 6:
                if getarg(1) == 0:
                    self.ptr = getarg(2)
                else:
                    self.ptr += 3
            elif opcode == 7:
                if getarg(1) < getarg(2):
                    p[p[self.ptr+3]] = 1
                else:
                    p[p[self.ptr+3]] = 0
                self.ptr += 4
            elif opcode == 8:
                if getarg(1) == getarg(2):
                    p[p[self.ptr+3]] = 1
                else:
                    p[p[self.ptr+3]] = 0
                self.ptr += 4
            else:
                # should never happen!
                print("Opcde error")
        self.halted = (p[self.ptr] % 100 == 99)
        return outp
