def exeprog(p,inp):
    
    def getarg(argno):
        if argno == 1 :
            amode = adrmode % 10
        elif argno == 2:
            amode = (adrmode // 10) % 10
        else:
            print("Argument number incorrect")
        if amode == 0:
            return p[p[ptr+argno]]
        elif amode == 1:
            return p[ptr+argno]
        else:
            print("Address mode error")
            
    ptr = 0
    inptr = 0
    outp = list()
    while p[ptr] % 100 != 99:
        opcode = p[ptr]%100
        adrmode = p[ptr] // 100
        if opcode == 1:
            p[p[ptr+3]] = getarg(1) + getarg(2)
            ptr += 4
        elif opcode == 2:
            p[p[ptr+3]] = getarg(1) * getarg(2)
            ptr += 4
        elif opcode == 3:
            p[p[ptr+1]] = inp[inptr]
            inptr += 1
            ptr += 2
        elif opcode == 4:
            outp.append(getarg(1))
            ptr += 2
        elif opcode == 5:
            if getarg(1) != 0:
                ptr = getarg(2)
            else:
                ptr += 3
        elif opcode == 6:
            if getarg(1) == 0:
                ptr = getarg(2)
            else:
                ptr += 3
        elif opcode == 7:
            if getarg(1) < getarg(2):
                p[p[ptr+3]] = 1
            else:
                p[p[ptr+3]] = 0
            ptr += 4
        elif opcode == 8:
            if getarg(1) == getarg(2):
                p[p[ptr+3]] = 1
            else:
                p[p[ptr+3]] = 0
            ptr += 4
        else:
            # should never happen!
            print("Opcde error") 
    return outp
