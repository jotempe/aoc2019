def exeprog(p, test):
	# test is the test location to be returned as function value
    ptr = 0
    while p[ptr] != 99:
        if p[ptr] == 1:
            p[p[ptr+3]] = p[p[ptr+1]] + p[p[ptr+2]]
        elif p[ptr] == 2:
            p[p[ptr+3]] = p[p[ptr+1]] * p[p[ptr+2]]
        else:
			# should never happen!
            print("error") 
        ptr = ptr + 4
    return p[test]
