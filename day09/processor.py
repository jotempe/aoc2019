class processor:
    def __init__(self, prog):
        self.mem = dict(zip(range(len(prog)), prog))  # working memory, initialised to a copy of the program
        self.ptr = 0                                 # current instruction pointer
        self.rbase = 0                               # relative base
        self.halted = False                          # true if the processor encountered halt instruction

    def run(self,inp):
        # Run until either a halt or an "unsatisfied" input instruction is encountered 
        
        def gm(adr):
            if adr in self.mem:
                return self.mem[adr]
            else:
                self.mem[adr] = 0           # create new memory word
                return 0
                
        def getadmod(argno):
            # Get address mode of given argument
            if argno == 1 :
                amode = adrmode % 10
            elif argno == 2:
                amode = (adrmode // 10) % 10
            elif argno == 3:
                amode = adrmode // 100
            else:
                print("Argument number incorrect")
                amode = 9
            return amode

        def getarg(argno):
            # Get value of the argument given as offset to current ptr.
            amode = getadmod(argno)
            if amode == 0:
                return gm(gm(self.ptr+argno))
            elif amode == 1:
                return gm(self.ptr+argno)
            elif amode == 2:
                return gm(gm(self.ptr+argno)+self.rbase)
            else:
                print("Address mode error")
                
        def putval(argno, value):
            # write value into memory pointed to by given argument
            amode = getadmod(argno)
            if amode == 0:
                self.mem[gm(self.ptr+argno)] = value
            elif amode == 2:
                self.mem[gm(self.ptr+argno)+self.rbase] = value
            else:                       # Address mode 1 not allowed on write
                print("Address mode error on putval")
            
                
        inpused = False         # Flag will be set by first intput instruction
        outp = list()             # will hold the output
        if self.halted:
            print("Error, processor already halted")
            return
            
        while gm(self.ptr) % 100 != 99:
            opcode = gm(self.ptr)%100
            adrmode = gm(self.ptr) // 100
            if opcode == 1:
                putval(3, getarg(1) + getarg(2))
                self.ptr += 4
            elif opcode == 2:
                putval(3, getarg(1) * getarg(2))
                self.ptr += 4
            elif opcode == 3:
                if inpused:
                    break
                else:
                    putval(1, inp)
                    inpused = True
                self.ptr += 2
            elif opcode == 4:
                outp.append(getarg(1))
                self.ptr += 2
            elif opcode == 5:
                if getarg(1) != 0:
                    self.ptr = getarg(2)
                else:
                    self.ptr += 3
            elif opcode == 6:
                if getarg(1) == 0:
                    self.ptr = getarg(2)
                else:
                    self.ptr += 3
            elif opcode == 7:
                if getarg(1) < getarg(2):
                    putval(3, 1)
                else:
                    putval(3, 0)
                self.ptr += 4
            elif opcode == 8:
                if getarg(1) == getarg(2):
                    putval(3, 1)
                else:
                    putval(3, 0)
                self.ptr += 4
            elif opcode == 9:
                self.rbase = self.rbase + getarg(1)
                self.ptr += 2
            else:
                # should never happen!
                print("Opcde error")
        self.halted = (gm(self.ptr) % 100 == 99)
        return outp
