class processor:
    def __init__(self, prog):
        self.mem = dict(zip(range(len(prog)), prog))  # working memory, initialised to a copy of the program
        self.ptr = 0                                 # current instruction pointer
        self.rbase = 0                               # relative base
        self.halted = False                          # true if the processor encountered halt instruction
        self.debug = False                           # Debug mode
        self.trace = False
        self.inpq = list()                           # Input queue for input instructions
        self.bkpts = list()                          # breakpoint locations
        self.watches = list()                        # watched locations (for modification)
    
    def run(self,inp):
        self.inpq += inp
        self.debug = False
        return self.go(0)

    def gm(self,adr):
        if adr in self.mem:
            return self.mem[adr]
        else:
            self.mem[adr] = 0           # create new memory word
            if self.debug:
                print("location",adr,"did not exist, created")
            return 0
            
    def getadmod(self,adrmode,argno):
        # Get address mode of given argument
        if argno == 1 :
            amode = adrmode % 10
        elif argno == 2:
            amode = (adrmode // 10) % 10
        elif argno == 3:
            amode = adrmode // 100
        else:
            print("Argument number incorrect")
            amode = 9
        return amode

    def getarg(self,adrmode, argno):
        # Get value of the argument given as offset to current ptr.
        amode = self.getadmod(adrmode,argno)
        if amode == 0:
            return self.gm(self.gm(self.ptr+argno))
        elif amode == 1:
            return self.gm(self.ptr+argno)
        elif amode == 2:
            return self.gm(self.gm(self.ptr+argno)+self.rbase)
        else:
            print("Address mode error")
            
    def putval(self, adrmode, argno, value):
        # write value into memory pointed to by given argument
        amode = self.getadmod(adrmode,argno)
        if amode == 0:
            adr = self.gm(self.ptr+argno)
        elif amode == 2:
            adr = self.gm(self.ptr+argno) + self.rbase
        else:                       # Address mode 1 not allowed on write
            print("Address mode error on putval")
            return
        if self.debug and (adr in self.watches):
            print("  loc {0:0=4} written by {1:36} new val: {2}".format(adr,self.disassemble(self.ptr,self.ptr)[0],value))
        self.mem[adr] = value
        
    def go(self,nstep):
        # Run until either a halt, an "unsatisfied" input instruction is encountered, or, if under debugger,
        # until a breakpoint is reached or requested number of instrctions have been exeuted.
                    
        outp = list()             # will hold the output
        if self.halted:
            print("Error, processor already halted")
            return
        if self.debug : print("  go for",nstep,"steps")    
        while self.gm(self.ptr) % 100 != 99:
            if self.trace:
                print("  executing:",self.disassemble(self.ptr,self.ptr)[0])
            opcode = self.gm(self.ptr)%100
            adrmode = self.gm(self.ptr) // 100
            if opcode == 1:
                self.putval(adrmode, 3, self.getarg(adrmode, 1) + self.getarg(adrmode, 2))
                self.ptr += 4
            elif opcode == 2:
                self.putval(adrmode, 3, self.getarg(adrmode, 1) * self.getarg(adrmode, 2))
                self.ptr += 4
            elif opcode == 3:
                if len(self.inpq) == 0:
                    if self.debug:
                        print("  unsatisfied input at",self.ptr)
                    break
                else:
                    self.putval(adrmode, 1, self.inpq.pop(0))
                self.ptr += 2
            elif opcode == 4:
                outp.append(self.getarg(adrmode,1))
                self.ptr += 2
            elif opcode == 5:
                if self.getarg(adrmode,1) != 0:
                    self.ptr = self.getarg(adrmode,2)
                else:
                    self.ptr += 3
            elif opcode == 6:
                if self.getarg(adrmode,1) == 0:
                    self.ptr = self.getarg(adrmode,2)
                else:
                    self.ptr += 3
            elif opcode == 7:
                if self.getarg(adrmode,1) < self.getarg(adrmode,2):
                    self.putval(adrmode, 3, 1)
                else:
                    self.putval(adrmode, 3, 0)
                self.ptr += 4
            elif opcode == 8:
                if self.getarg(adrmode,1) == self.getarg(adrmode,2):
                    self.putval(adrmode, 3, 1)
                else:
                    self.putval(adrmode, 3, 0)
                self.ptr += 4
            elif opcode == 9:
                self.rbase = self.rbase + self.getarg(adrmode,1)
                self.ptr += 2
            else:
                # should never happen!
                print("Opcde error")
            if self.debug and self.ptr in self.bkpts:
                print("  breakpoint at",self.ptr)
                break
            if self.debug and nstep==1:
                break
            if nstep>0 : nstep -= 1

        self.halted = (self.gm(self.ptr) % 100 == 99)
        if self.debug and self.halted :
            print("  Halted at",self.ptr)          
        return outp
        
    def printhelp(self):
        print("input debugger command:")
        print("  a start [end] - examine memory as ASCII")
        print("  b [addr] [-addr] - examine/add/remove breakpoint")
        print("  d [start] [end] - disassemble instructions from start to end")
        print("  g - run until beakpoint or halt")
        print("  i data... - examine/append to input queue")
        print("  m start [end] - examine memory as integers")
        print("  p [addr] - examine/set instruction pointer")
        print("  q - quit debugger")
        print("  r [addr] - examine/set relative base address")
        print("  s [count] - step for count (default 1) instructions")
        print("  t [1|0] - trace mode on/off")
        print("  w [addr] [-addr] - examine/add/remove wached location")
    
    def debugger(self):
    
        self.debug = True
        while True:
            commandline = input("dbg> ")
            command = commandline.split()
            if len(command) == 0:
                self.printhelp()
                continue
            cmd = command.pop(0)
            if cmd == 'b':         # add/remove breakpoint
                if len(command) == 0:
                    print("  breakpoints: ",[b for b in self.bkpts])
                else:
                    for a in command:
                        adr = int(a)
                        if adr<0:
                            if -adr in self.bkpts:
                                self.bkpts.remove(-adr)
                        else:
                            if adr not in self.bkpts:
                                self.bkpts.append(adr)
            elif cmd == 'w':         # add/remove breakpoint
                if len(command) == 0:
                    print("  watched locs: ",[b for b in self.watches])
                else:
                    for a in command:
                        adr = int(a)
                        if adr<0:
                            if -adr in self.watches:
                                self.watches.remove(-adr)
                        else:
                            if adr not in self.watches:
                                self.watches.append(adr)

            elif cmd == 'q':          # quit debugger
                break
            elif cmd == 't':
                if len(command)>0 and command[0]=='off':
                    self.trace = False
                else:
                    self.trace = True
            elif cmd == 'p':
                if len(command)==0:
                    print("  instruction pointer:", self.ptr)
                else:
                    self.ptr = int(command[0])
            elif cmd == 'r':
                if len(command)==0:
                    print("  relative base address:", self.rbase)
                else:
                    self.rbase = int(command[0])
            elif cmd == 'm':
                end = -1
                if len(command) > 0:
                    start = int(command[0])
                if len(command) > 1:
                    end = int(command[1])
                if end>0 :
                    for i in range(start,end):
                        print('  {0:0=4}: {1}'.format(i,self.gm(i)))
                else:
                    print('  {0:0=4}: {1}'.format(start,self.gm(start)))
            elif cmd == 'a':
                end = -1
                if len(command) > 0:
                    start = int(command[0])
                if len(command) > 1:
                    end = int(command[1])
                if end>0 :
                    for i in range(start,end):
                        if self.gm(i) in range(32,127) or self.gm(i) == 10:
                            print(chr(self.gm(i)),end='')
                        else:
                            print('.',end='')
                    print()
                else:
                    print('  {0:0=4}: {1}'.format(start,chr(self.gm(start))))
            elif cmd == 's':
                if len(command)>0:
                    nstep = int(command[0])
                else:
                    nstep = 1
                out = self.go(nstep)
                if len(out) > 0:
                    print("  output:",out)
            elif cmd == 'g':
                out = self.go(0)
                if len(out) > 0:
                    print("  output:",out)
            elif cmd == 'd':
                if len(command) > 0:
                    start = int(command[0])
                else:
                    start = self.ptr
                if len(command) > 1:
                    end = int(command[1])
                else:
                    end = start
                out = self.disassemble(start,end)
                for line in out: print("  {0}".format(line))
            elif cmd == 'i':
                if len(command) == 0:
                    print ("  input queue:", self.inpq)
                else:
                    self.inpq += [int(c) for c in command]
                
    def disassemble(self,start,end):

        def addpar(ptr,offset,adrmode):
            amode = self.getadmod(adrmode,offset)
            if amode == 0:
                res = '@{0:0=4}'
            elif amode == 1:
                res = '#{0}'
            elif amode == 2:
                res = '{0},R'
            else:
                return '???'
            return res.format(self.gm(ptr+offset))

        code = True
        ptr = start
        out = []
        while ptr <= end:
            instr = self.gm(ptr)
            if instr <= 0 or instr >= 22209 :    # this can't be code
                code = False
                break
            line = '{0:0=4}:  '.format(ptr)      # start with instruction address
            opcode = instr % 100
            amode = instr // 100
            if opcode == 1:
                line += 'ADD   '
                line += addpar(ptr,1,amode)
                line += '; '
                line += addpar(ptr,2,amode)
                line += ' >'
                line += addpar(ptr,3,amode)
                ptr += 4
            elif opcode == 2:
                line += 'MUL   '
                line += addpar(ptr,1,amode)
                line += '; '
                line += addpar(ptr,2,amode)
                line += ' >'
                line += addpar(ptr,3,amode)
                ptr += 4
            elif opcode == 3:
                line += 'INP  '
                line += '>'
                line += addpar(ptr,1,amode)
                ptr += 2
            elif opcode == 4:
                line += 'OUT   '
                line += addpar(ptr,1,amode)            
                ptr += 2
            elif opcode == 5:
                line += 'CJT   '       # Conditional jump if true
                line += addpar(ptr,1,amode)
                line += ' ^'
                line += addpar(ptr,2,amode)
                ptr += 3
            elif opcode == 6:
                line += 'CJF   '      # Conditional jump if false
                line += addpar(ptr,1,amode)
                line += ' ^'
                line += addpar(ptr,2,amode)
                ptr += 3
            elif opcode == 7:
                line += 'TLT   '      # Test less than
                line += addpar(ptr,1,amode)
                line += '; '
                line += addpar(ptr,2,amode)
                line += ' >'
                line += addpar(ptr,3,amode)            
                ptr += 4
            elif opcode == 8:
                line += 'TEQ   '       # Test equal
                line += addpar(ptr,1,amode)
                line += '; '
                line += addpar(ptr,2,amode)
                line += ' >'
                line += addpar(ptr,3,amode)            
                ptr += 4
            elif opcode == 9:
                line += 'ADJR  '      # Adjust relative base
                line += addpar(ptr,1,amode)
                ptr += 2
            elif opcode == 99:
                line += 'HALT'
                ptr += 1
            else:
                code = False      # unknown opcode, we are probably trying to decode data
                break
            out.append(line)
        return out
                        