class wire:
	def __init__ (self):
		self.segs = list()
		
	def runwire (self, route):
		xcurr = 0
		ycurr = 0
		for e in route:
			dir = e[0]
			len = int(e[1:])
			if dir == 'L':
				self.segs.append(wiresegment('H-', ycurr, xcurr-len, xcurr))
				xcurr = xcurr - len
			elif dir == 'R':
				self.segs.append(wiresegment('H+', ycurr, xcurr, xcurr+len))
				xcurr = xcurr + len
			elif dir == 'U':
				self.segs.append(wiresegment('V+', xcurr, ycurr, ycurr+len))
				ycurr = ycurr + len
			elif dir == 'D':
				self.segs.append(wiresegment('V-', xcurr, ycurr-len, ycurr))
				ycurr = ycurr - len
			else :
				print('error!')
				exit()
				
	def lento(self, point):
		length = 0
		x = point[0]
		y = point[1]
		for s in self.segs:
			if s.orientation[0] == 'H' and s.fixcoord == y and x >= s.start and x <= s.end :
				if s.orientation[1] == '+' :
					length = length + x - s.start
				else :
					length = length + s.end - x
				break
			elif s.orientation[0] == 'V' and s.fixcoord == x and y >= s.start and y <= s.end :
				if s.orientation[1] == '+' :
					length = length + y - s.start
				else :
					length = length + s.end - y
				break
			else:
				length = length + s.end - s.start
		return length