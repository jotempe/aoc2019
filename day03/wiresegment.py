class wiresegment:
	def __init__ (self, ort, fix, low, high):
		self.orientation = ort
		self.fixcoord = fix
		self.start = low
		self.end = high
		
	def __str__ (self):
		return self.orientation + " segment at " + str(self.fixcoord) + " from " + str(self.start) + " to " + str(self.end)
		
	def __repr__ (self):
		return str(self)
		
	def crosspoint (self, other):
		if self.orientation[0] == other.orientation[0]:
			return
		if (self.fixcoord < other.start) or (self.fixcoord > other.end) :
			return
		if (other.fixcoord < self.start) or (other.fixcoord > self.end) :
			return
		if self.orientation[0] == 'V' :
			return (self.fixcoord, other.fixcoord)
		else :
			return (other.fixcoord, self.fixcoord)
			
